This project demonstrates JDBC techniques on the persons table in an H2 memory datasource.
Each technology is presented in a different CommandLinRunner.
CommandLineRunners use methods from the PersonJdbcTemplateRepository.