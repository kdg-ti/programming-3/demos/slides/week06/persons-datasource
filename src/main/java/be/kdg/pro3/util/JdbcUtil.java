package be.kdg.pro3.util;

import java.sql.ResultSet;

public class JdbcUtil {
	public static String resultSetToString(ResultSet resultSet) {
		StringBuilder sb = new StringBuilder();
		try {
			int rows = resultSet.getMetaData().getColumnCount();
			while (resultSet.next()) {
				for (int i = 1; i <= rows; i++) {
					sb.append(resultSet.getString(i)).append(i<rows?" ":"\n");
				}
			}
		} catch (Exception e) {
			System.err.println("Error printing resultset " + resultSet);
			e.printStackTrace();
		}
		return sb.toString();
	}
}
