package be.kdg.pro3.persons_datasource;

import be.kdg.pro3.util.JdbcUtil;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.Random;

@Component
@Order(20)
public class TransactionRunner implements CommandLineRunner {
	@Override
	public void run(String... args) throws Exception {
		System.out.println("\n>> JDBC transactions");
		try (Connection connection = DriverManager.getConnection("jdbc:h2:mem:personsdb","sa","")) {
			connection.setAutoCommit(false);
			try (Statement statement = connection.createStatement()) {
				try {
					statement.executeUpdate("""
						  INSERT INTO PERSONS(NAME, FIRSTNAME, REMARK) 
						  VALUES('Truus','Trampoline','Lastly through a hogshead of real fire!')
						""");
					statement.executeUpdate("DELETE FROM PERSONS WHERE NAME LIKE '%ER'");
					if (new Random().nextBoolean()) throw new SQLException("Problem!");
					System.out.println("No problem, inserting and deleting...");
					connection.commit();
				} catch (SQLException e) {
					System.out.println("Problem, rolling back delete and insert!");
					connection.rollback();
				}
				System.out.println(JdbcUtil.resultSetToString(statement.executeQuery("SELECT * FROM PERSONS")));
			} // end statement
		}
	}
}
