package be.kdg.pro3.persons_datasource;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.sql.*;

@Component
@Order(10)
public class DataSourceRunner implements CommandLineRunner {
	@Override
	public void run(String... args) throws Exception {
		System.out.println("\n>> JDBC using a datasource");
		try (Connection connection = DriverManager.getConnection("jdbc:h2:mem:personsdb", "sa", "");
		     Statement statement = connection.createStatement()) {
			try (ResultSet resultSet = statement.executeQuery("SELECT * FROM PERSONS")) {
				while (resultSet.next()) {
					System.out.println(
						resultSet.getString("NAME") + " " + resultSet.getString("FIRSTNAME"));
				}
			}
		}
	}
}
