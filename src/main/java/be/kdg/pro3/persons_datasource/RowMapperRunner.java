package be.kdg.pro3.persons_datasource;

import be.kdg.pro3.persons_datasource.domain.Person;
import be.kdg.pro3.persons_datasource.repository.PersonRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(40)
public class RowMapperRunner implements CommandLineRunner {
	private PersonRepository personRepository;

	public RowMapperRunner(	 PersonRepository personRepository) {
		this.personRepository = personRepository;
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("\n>> RowMapperRunner");
		System.out.println(personRepository.findByName("JONES"));
		System.out.println(personRepository.findById(4));
		System.out.println(personRepository.findByFirstName("JACK"));
		System.out.println(personRepository.save(new Person("Henderson","Joe","Late of Pablo Fanque's fair")));
	}

}

