package be.kdg.pro3.persons_datasource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonsDatasourceApplication  {

	public static void main(String[] args) {
		SpringApplication.run(PersonsDatasourceApplication.class, args);
	}

}