package be.kdg.pro3.persons_datasource.repository;

import be.kdg.pro3.persons_datasource.domain.Person;

import java.util.List;

public interface PersonRepository {
	List<Person> findByName(String name);
	Person findById(int id);

	List<Person> findByFirstName(String firstName);

	Person save(Person person);
}
