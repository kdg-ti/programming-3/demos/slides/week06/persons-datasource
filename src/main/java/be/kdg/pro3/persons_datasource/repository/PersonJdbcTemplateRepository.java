package be.kdg.pro3.persons_datasource.repository;

import be.kdg.pro3.persons_datasource.domain.Person;
import org.springframework.jdbc.core.*;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.*;

@Repository
public class PersonJdbcTemplateRepository implements PersonRepository {

	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert personInserter;

	public PersonJdbcTemplateRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
		personInserter = new SimpleJdbcInsert(jdbcTemplate).withTableName("PERSONS")
			.usingGeneratedKeyColumns("ID");
	}

	@Override
	public List<Person> findByName(String name) {
		return jdbcTemplate.query("SELECT * FROM PERSONS WHERE NAME = ?",
			(rs, rowNum) -> new Person(rs.getInt("id"),
				rs.getString("name"),
				rs.getString("firstname"),
				rs.getString("remark")),
			name);
	}

	@Override
	public Person findById(int id) {
		return jdbcTemplate.queryForObject("SELECT * FROM PERSONS WHERE ID = ?",
			this::mapRow,
			id);
	}

	@Override
	public List<Person> findByFirstName(String firstName) {
		return jdbcTemplate.query("SELECT * FROM PERSONS WHERE FIRSTNAME = ?",
			new BeanPropertyRowMapper<>(Person.class),
			firstName);
	}

	@Override
	public Person save(Person person) {
		PreparedStatementCreatorFactory pscf = new
			PreparedStatementCreatorFactory("INSERT INTO PERSONS(NAME, FIRSTNAME,REMARK) VALUES (?, ?,?)",
			Types.VARCHAR,
			Types.VARCHAR,
			Types.VARCHAR);
		pscf.setReturnGeneratedKeys(true);
		PreparedStatementCreator psc =
			pscf.newPreparedStatementCreator(List.of(person.getName(),
				person.getFirstName(),
				person.getRemark()));
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(psc, keyHolder);
		person.setId(keyHolder.getKey().intValue());
		return person;
	}

	public Person saveInserter(Person person) {
		int personId = personInserter.executeAndReturnKey(Map.of(
			"NAME", person.getName(),
			"FIRSTNAME", person.getFirstName(),
			"REMARK", person.getRemark())
		).intValue();
		person.setId(personId);
		return person;
	}

	private Person mapRow(ResultSet rs, int i) throws SQLException {
		return new Person(rs.getInt("id"),
			rs.getString("name"),
			rs.getString("firstname"),
			rs.getString("remark"));
	}


}
