package be.kdg.pro3.persons_datasource;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
@Order(30)
public class JdbcTemplateRunner implements CommandLineRunner {
	private JdbcTemplate jdbcTemplate;

	public JdbcTemplateRunner(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	private  void handleRow(ResultSet rs) throws SQLException {
		System.out.println(rs.getString("ID") + " " + rs.getString("NAME"));
	}

	@Override
	public void run(String... args)  {
		System.out.println("\n>> JDBC template");
		jdbcTemplate.query("SELECT * FROM PERSONS",
			(RowCallbackHandler) line -> handleRow(line));
	}

}
