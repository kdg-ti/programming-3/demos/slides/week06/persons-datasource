package be.kdg.pro3.persons_datasource.domain;

public class Person {
	private int id;
	private String name;
	private String firstName;
	private String remark;

	public Person(int id, String name, String firstName, String remark) {
		this.id = id;
		this.name = name;
		this.firstName = firstName;
		this.remark = remark;
	}

	public Person(String name, String firstName, String remark) {
		this(0, name, firstName, remark);
	}

	public Person() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "Person{" +
			"id=" + id +
			", name='" + name + '\'' +
			", firstName='" + firstName + '\'' +
			", remark='" + remark + '\'' +
			'}';
	}
}
